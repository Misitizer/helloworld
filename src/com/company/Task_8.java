package com.company;

import java.util.Scanner;

public class Task_8 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();
        equals(num);
    }

    public static void equals(int num){
        String str = String.valueOf(num);
        if ((str.charAt(0)==str.charAt(1)) || (str.charAt(1)==str.charAt(2)) || (str.charAt(2)==str.charAt(0))){
            System.out.println(true);
        }
        else {
            System.out.println(false);
        }
    }
}
