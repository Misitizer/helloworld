package com.company;

import java.util.Scanner;

public class S03_T02 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        stars(str);
    }

    public static void stars(String str){
        int len = str.length();
        for (int i = 0; i<len; i++){
            str = "*"+str+"*";
        }
        System.out.println(str);
    }
}