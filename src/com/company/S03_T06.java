package com.company;

import java.util.Scanner;

public class S03_T06 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        sumString(str);

    }

    public static void sumString(String str){
        int sum = 0;
        char[] nums = str.toCharArray();
        for (char num: nums){
            sum = sum + (num - '0');
        }
        System.out.println(sum);
    }
}
