package com.company;

import java.util.Scanner;

public class S03_T05 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        stringPalindrome(str);

    }

    public static void stringPalindrome(String str){
        String reverse_str = new StringBuilder(str).reverse().toString();
        if (str.equals(reverse_str)){
            System.out.println(true);
        }
        else{
            System.out.println(false);
        }
    }
}
