package com.company;

import java.util.Scanner;

public class S03_T01 {
    public static void main(String[] args){
        Scanner in  = new Scanner(System.in);
        String str = in.nextLine();
        reverse(str);
    }

    public static void reverse(String str){
        int len = str.length();
        String res = "";
        for (int i = 0; i<len; i++){
            res = str.charAt(i) + res;
        }
        System.out.println(res);
    }
}
