package com.company;

import java.util.Scanner;

public class Task_7 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();
        //stringPalindrome(num);
        //tripPalindrome(num);
        charPalindrome(num);
    }

    public static void charPalindrome(int num){
        String str = String.valueOf(num);
        if (str.charAt(0) == str.charAt(2)){
            System.out.println(true);
        }
        else{
            System.out.println(false);
        }
    }

    public static void tripPalindrome(int num){ // метод для трехзначных чисел не через строки
        int a, b, c;
        a = num/100;
        b = num%100/10;
        c = num%10;
        int reverseNum = c*100+b*10+a;
        if (reverseNum == num){
            System.out.println(true);
        }
        else{
            System.out.println(false);
        }
    }

    public static void stringPalindrome(int num){ // метод универсальный через строки
        String str = String.valueOf(num);
        String reverse_str = new StringBuilder(str).reverse().toString();
        if (str.equals(reverse_str)){
            System.out.println(true);
        }
        else{
            System.out.println(false);
        }
    }
}
