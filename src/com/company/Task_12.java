package com.company;

import java.util.Scanner;

public class Task_12 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();
        //buildPyramid(num);
        cyclePyramid(num);
    }

    public static void cyclePyramid(int num){ // метод с циклами
        for (int i = 1; i<=num; i++){
            for (int j = 1; j<=i; j++){
               System.out.print("* ");
            }
            System.out.println();
        }
    }

    public static void buildPyramid(int num){ // метод со строками
        StringBuilder builder = new StringBuilder();
        for(int i = 1; i<=num; i++){
            builder.append("* ");
            System.out.println(builder.toString());
        }
    }
}
