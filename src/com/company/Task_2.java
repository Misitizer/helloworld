package com.company;

import java.util.Scanner;

public class Task_2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double y;
        double x = in.nextDouble();
        if (x>0) {
            y = Math.pow(Math.sin(x),2);
        }
        else {
            y = 1-2*Math.sin(Math.pow(x,2));
        }
        System.out.println(y);
    }
}
