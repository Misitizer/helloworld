package com.company;

import java.util.Scanner;

public class Task_9 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double x = in.nextDouble();
        mathFunc(x);
    }

    public static void mathFunc(double x){
        double y;
        if (x<=0) {
            y = 0;
        }
        else if (x>0 && x<=1){
            y = x;
        }
        else {
            y = x*x;
        }
        System.out.println(y);
    }
}
