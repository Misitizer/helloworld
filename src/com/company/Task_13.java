package com.company;

import java.util.Scanner;

public class Task_13 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int weight = in.nextInt();
        pyramid(weight);
    }

    public static void pyramid(int weight){
        if (weight % 2 == 0) {
            weight++;
        }
        int height = (weight + 1) / 2;
        for (int i = 0; i < height; i++) {
            for (int j = 1; j < height - i; j++) {
                System.out.print(" ");
            }
            for (int k = 1; k <= i * 2 + 1; k++) {
                System.out.print("*");
            }
            if (i != height - 1) {
                System.out.println();
            }
        }
    }
}