package com.company;

import java.util.Scanner;

public class S03_T03 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        String s = in.nextLine();
        countSymbol(str, s);
    }

    public static void countSymbol(String str, String s){
        int count = 0;
        for (int i = 0; i<str.length(); i++ ){
            if (str.charAt(i) == s.charAt(0)){
                count++;
            }
        }
        System.out.println(count);
    }
}
