package com.company;

import java.util.Scanner;

public class Task_5 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int r = in.nextInt();
        area(a, r);
    }

    public static void area(int a, int r){
        if (Math.pow(a,2) > Math.PI*Math.pow(r,2)){
            System.out.println("Площадь квадрата больше, чем площадь круга");
        }
        else if (Math.pow(a,2) == Math.PI*Math.pow(r,2)){
            System.out.println("Площади равны");
        }
        else{
            System.out.println("Площадь круга больше, чем площадь квадрата");
        }
    }
}
